package com.Lab2_SD3.Market.Controllers;

import com.Lab2_SD3.Market.Dtos.ProductDto;
import com.Lab2_SD3.Market.Entities.Product;
import com.Lab2_SD3.Market.Repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository repository;

    // Endpoint para criar um novo produto
    @PostMapping("/products")
    public ResponseEntity<Product> create(@RequestBody ProductDto dto) {
        Product prd = new Product();
        prd.setName(dto.name);
        prd.setPrice(dto.price);
        prd.setQuantity(dto.quantity);
        prd.setDescription(dto.description);

        repository.save(prd);
        return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(prd));
    }

    // Endpoint para listar todos os produtos
    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> products = repository.findAll();
        return ResponseEntity.ok().body(products);
    }

    // Endpoint para excluir um produto por ID
    @DeleteMapping("/products/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable UUID id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // Endpoint para atualizar os detalhes de um produto existente por ID
    @PutMapping("/products/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable UUID id, @RequestBody ProductDto dto) {
        if (repository.existsById(id)) {
            Product prd = repository.getOne(id);
            prd.setName(dto.name);
            prd.setPrice(dto.price);
            prd.setQuantity(dto.quantity);
            prd.setDescription(dto.description);
            repository.save(prd);
            return ResponseEntity.ok().body(prd);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // Endpoint para consultar produtos por nome
    @GetMapping("/products/searchByName")
    public ResponseEntity<List<Product>> searchByName(@RequestParam String name) {
        List<Product> products = repository.findByNameContaining(name);
        return ResponseEntity.ok().body(products);
    }

    // Endpoint para consultar produtos por faixa de preço
    @GetMapping("/products/searchByPriceRange")
    public ResponseEntity<List<Product>> searchByPriceRange(@RequestParam Double minPrice, @RequestParam Double maxPrice) {
        List<Product> products = repository.findByPriceBetween(minPrice, maxPrice);
        return ResponseEntity.ok().body(products);
    }

    // Endpoint para consultar produtos por disponibilidade em estoque
    @GetMapping("/products/searchByAvailability")
    public ResponseEntity<List<Product>> searchByAvailability(@RequestParam boolean available) {
        List<Product> products = repository.findByQuantityGreaterThan(0);
        return ResponseEntity.ok().body(products);
    }
}
