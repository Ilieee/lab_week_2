package com.Lab2_SD3.Market.Entities;

import jakarta.persistence.*;

@Entity
@Table(name="TB_PRODUCTS")
public class Product extends AbstractEntity{

    @Column(name="NM_PRODUCT")
    private String name;
    @Column(name="VL_PRICE")
    private Double price;
    @Column(name="DS_PRODUCT")
    private String description;
    @Column(name="NR_QUANTITY")
    private int quantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
