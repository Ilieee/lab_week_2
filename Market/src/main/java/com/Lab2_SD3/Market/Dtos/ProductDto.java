package com.Lab2_SD3.Market.Dtos;

public class ProductDto {

    public String name;
    public Double price;
    public int quantity;
    public String description;
}
