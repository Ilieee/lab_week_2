# Funcionalidades

Este projeto é uma aplicação de gerenciamento de produtos para um mercado. Ele fornece as seguintes funcionalidades:

- **Criar Produto**: Permite adicionar novos produtos ao sistema, incluindo nome, preço, quantidade e descrição.
- **Listar Produtos**: Mostra todos os produtos cadastrados no sistema.
- **Excluir Produto**: Remove um produto existente com base no seu identificador único.
- **Atualizar Produto**: Atualiza os detalhes de um produto já existente.
- **Consultar por Nome**: Permite buscar produtos pelo nome.
- **Consultar por Faixa de Preço**: Recupera produtos cujos preços estejam dentro de uma faixa específica.
- **Consultar por Disponibilidade**: Recupera produtos com quantidade em estoque maior que zero.

